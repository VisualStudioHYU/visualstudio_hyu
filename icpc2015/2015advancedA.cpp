/* Pizza Party */

#include <iostream>

using namespace std;

int main () {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {

        int n; cin >> n;

        if (n == 1) {

            cout << "0" << endl;
        }

        else if (n % 2 == 0) {

            cout << n / 2 << endl;
        }

        else {

            cout << n << endl;
        }

    }

    return 0;
}
