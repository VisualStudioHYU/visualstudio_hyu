/* Hexagon  */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() 
{
   int T, t, i;
   int byun[6];
   int max=0, secondmax=0;
   int result;
   scanf("%d", &T);

   for (t = 0; t < T; t++)
   {
      for (i = 0; i < 6; i++)
      {
         scanf("%d", &byun[i]);
      }
      if (byun[0] > byun[1])
      {
         max = byun[0];
         secondmax = byun[1];
      }
      else
      {
         max = byun[1];
         secondmax = byun[0];
      }
      result = (max*max) + (secondmax*secondmax) + (4 * max*secondmax);
      printf("%d\n", result);
   }
   return 0;
}