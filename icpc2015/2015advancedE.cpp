/* Binary Strawberry */

#include <iostream>
#include <vector>
#include <string.h>

using namespace std;

int main () {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {

        int players;
        int dis;
        int me;
        int tmp;

        cin >> players >> dis >> me;

        int sum = dis + 1;
        int *arr = new int[me];

        arr[0] = sum;
        tmp = sum;

        for (int i = 1; i < me; i++) {

            sum += players;

            if (sum > 15) {

                tmp = sum - 15;
                sum = 15 - tmp;
                players *= -1;
            }

            if (sum < 1) {

                tmp = 1 - sum;
                sum = 1 + tmp;
                players *= -1;

            }

            arr[i] = sum;
        }

        for (int i = 0; i < me; i++) {

            switch (arr[i]) {

            case 1:

                cout << "mmmS" << endl;
                break;

            case 2:

                cout << "mmSm" << endl;
                break;

            case 3:

                cout << "mmSS" << endl;
                break;

            case 4:

                cout << "mSmm" << endl;
                break;

            case 5:

                cout << "mSmS" << endl;
                break;

            case 6:

                cout << "mSSm" << endl;
                break;

            case 7:

                cout << "mSSS" << endl;
                break;

            case 8:

                cout << "Smmm" << endl;
                break;

            case 9:

                cout << "SmmS" << endl;
                break;

            case 10:

                cout << "SmSm" << endl;
                break;

            case 11:

                cout << "SmSS" << endl;
                break;

            case 12:

                cout << "SSmm" << endl;
                break;

            case 13:

                cout << "SSmS" << endl;
                break;

            case 14:

                cout << "SSSm" << endl;
                break;

            case 15:

                cout << "SSSS" << endl;
                break;

            }
        }

        delete[] (arr);
    }

    return 0;
}