/* 아쿠아맨 */

#include <iostream>

using namespace std;

int main () {

    int T; cin >> T;
    int N, M;

    for (int t = 0; t < T; t++) {
        cin >> N, M;

        int sum = 0, temp = 0;
        int *arr = new int[N];
        int start = 0;
        int end, start_H, end_H, end_imsi_H;
        int end_imsi = 0;

        for (int i = 0; i < N; i++) {
            cin >> arr[i];
        }

        for (int i = 0; i < N; i++) {
            start_H = arr[start];
            end_H = arr[start + 1];

            if (start_H > end_H) {
                if (end_imsi_H < end_H) {
                    end_imsi_H = end_H;
                    end_imsi = end;
                }
            }

            if (i == N - 1) {
                end = end_imsi;
                end_H = arr[end];
                for (int j = 0; j < end; j++) {
                    sum += start_H - end_H;
                }

                start = end;
                end = end + 1;

                if (end + 1 > N) {
                    break;
                }
            }
            if (start_H < end_H) {
                end_imsi_H = end_H;
                end_imsi = end;
            }

            if (start_H == end_H) {
                end = end_imsi;
                end_H = arr[end];
                for (int j = 0; j < end; j++) {
                    sum += start_H - end_H;
                }

                start = end;
                end = end + 1;

                if (end + 1 > N) {
                    break;
                }

            }

            if (sum >= M) {
                cout << "SWIM" << endl;
            }
            else if (sum < M) {
                cout << "DUMPED" << endl;
            }
            else if (sum == 0) {
                cout << "FAIL" << endl;
            }
        }
    }

    return 0;
}
