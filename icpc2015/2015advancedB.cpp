/* 피겨 스케이팅 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int cmp (const void *a, const void *b) {

    int arg1 = *(const int*)a;
    int arg2 = *(const int*)b;

    if (arg1 < arg2) return -1;
    if (arg1 > arg2) return 1;
}

int main () {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {

        int N; cin >> N;

        int *gisul = new int[N];
        int *yesul = new int[N];
        int *sum1 = new int[N];
        int *sum2 = new int[N];
        int *tmp = new int[N];
        int *seq = new int[N];
        int *result = new int[N];

        for (int n = 0; n < N; n++) {

            int yesul, gisul;
            cin >> yesul >> gisul;
            sum1[n] = yesul + gisul;
            tmp[n] = sum1[n];
        }

        qsort (tmp, N, sizeof (int), cmp);

        for (int n = 0; n < N; n++) {

            for (int m = 0; m < N; m++) {

                if (tmp[n] == sum1[m]) {

                    seq[n] = m;
                    break;
                }
            }
        }

        for (int n = 0; n < N; n++) {

            int yesul, gisul;
            cin >> yesul >> gisul;
            sum2[seq[n]] = yesul + gisul;
            result[n] = tmp[n] + sum2[seq[n]];
        }

        qsort (result, N, sizeof (int), cmp);

        cout << result[N - 1] << endl;
    }

    return 0;
}

/*
1
5
13 16
27 25
23 25
16 12
9 1
2 7
20 19
23 16
0 6
22 16

*/
