/* 킹카의 고민 */

#include <iostream>

using namespace std;

int main () {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {

        int N; cin >> N;

        if (N == 0) {

            cout << "0" << endl;
            continue;
        }

        int *arr = new int[N * 2];

        for (int i = 0; i < N; i++) {

            cin >> arr[i];
            arr[2 * N - 1 - i] = arr[i] - 1;
        }

        long long result = 1;

        for (int i = 0; i < N; i++) {

            result = result * arr[i] * arr[2 * N - 1 - i];
        }

        cout << result << endl;
    }

    return 0;
}
