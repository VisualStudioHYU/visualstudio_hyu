/* 퀸카의 고민 */

#include <iostream>
#include <stdlib.h>

using namespace std;

int main () {

    int testCase;

    cin >> testCase;

    for (int i = 0; i < testCase; i++) {

        int yunseongToAjeong, yunseongToAjeongSum = 0;
        int junbeomToAjeong, junbeomToAjeongSum = 0;

        for (int j = 0; j < 7; j++) {

            cin >> junbeomToAjeong;
            cin >> yunseongToAjeong;
            junbeomToAjeongSum += junbeomToAjeong;
            yunseongToAjeongSum += yunseongToAjeong;
        }

        if (yunseongToAjeongSum == junbeomToAjeongSum) {

            cout << "Seo A Jeong solo" << endl;
        }

        else if (yunseongToAjeongSum > junbeomToAjeongSum) {

            cout << "Cute Yunseong I love you" << endl;
        }

        else {

            cout << "God Junbeom bless you" << endl;
        }
    }

    return 0;
}

/* 
3
2 1
4 3
6 5
8 7
10 9
12 11
14 13
2 7
3 10
5 2
30 29
50 34
40 57
10 1
2 3
4 5
6 7

*/