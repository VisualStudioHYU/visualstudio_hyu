#include <iostream>
#include <cmath>
	
using namespace std;

int main () {

	int T;
	cin >> T;

	for (int t = 0; t < T; t++) {

		double max = INT_MIN;
		double min = INT_MAX;
		int a, b, p, q;
		int team;
		int game;

		cin >> team >> game;

		int arr[1001][2] = {0};
		double arr2[1001];

		for (int i = 0; i < game; i++) {

			cin >> a >> b >> p >> q;
			arr[a][0] += p;
			arr[a][1] += q;
			arr[b][0] += q;
			arr[b][1] += p;
		}

		for (int i = 1; i < team + 1; i++) {

			if (arr[i][0] == 0 && arr[i][1] == 0)
				continue;

			arr2[i] = (double)((double)pow (arr[i][0], 2) / ((double)pow (arr[i][0], 2) + (double)pow (arr[i][1], 2)));
		}

		for (int i = 1; i < team + 1; i++) {

			if (max < arr2[i])
				max = arr2[i];

			if (min > arr2[i])
				min = arr2[i];
		}

		max *= 1000;
		min *= 1000;

		cout << (int)max << endl << (int)min << endl;
	}

	return 0;
}
/*
2
3 5
1 2 3 5
1 3 10 1
1 2 0 7
2 3 9 3
3 2 4 5
4 6
1 2 0 11
1 3 17 13
1 4 17 1
2 3 7 12
2 4 19 17
3 4 17 0
*/
