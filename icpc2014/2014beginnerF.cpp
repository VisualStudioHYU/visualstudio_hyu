/* 홀짝 */

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

int main () {

    int testCase;

    cin >> testCase;

    for (int z = 0; z < testCase; z++) {

        int n;
        cin >> n;

        int *m = new int[n];
        int oddCount = 0;
        int evenCount = 0;

        vector <int> odds;
        vector <int> evens;

        for (int i = 0; i < n; i++) {

            cin >> m[i];

            if (m[i] % 2 == 0)
                evens.push_back (m[i]);

            else
                odds.push_back (m[i]);
        }

        int oddSum = accumulate (odds.begin (), odds.end (), 0);
        int evenSum = accumulate (evens.begin (), evens.end (), 0);

        cout << evenSum << " " << oddSum << endl;
    }

    return 0;
}

/*
2
5
4 6 9 11 14
7
5 5 5 5 5 5 5

*/