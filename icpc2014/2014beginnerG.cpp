/* 중앙값 */

#include <iostream>
#include <numeric>
#include <algorithm>
#include <vector>

using namespace std;

int main () {

    int C; cin >> C;

    for (int c = 0; c < C; c++) {

        vector <int> v;

        for (int i = 0; i <= c; i++) {

            int m; cin >> m;

            v.push_back (m);
        }

        sort (v.begin (), v.end (), less <int> ());

        int result = v[c / 2];
        cout << result << endl;
    }

    return 0;
}

/* 
3
177
178 177
175 180 165

*/