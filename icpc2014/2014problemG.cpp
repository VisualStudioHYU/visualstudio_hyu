#include <iostream>

using namespace std;

int main () {

	int testCase;

	cin >> testCase;
	 
	int i;
	int number = 1;

	for (i = 0; i < testCase; i++) {

		int n;
		cin >> n;

		int *xarr = new int[n];
		int *yarr = new int[n];
		int *s = new int[n];
		int *num = new int[n];
		int *valid = new int[n];
		int *xeq = new int[n];
		int *yeq = new int[n];
		int xl = 0;
		int yl = 0;
		int xIdx;
		int yIdx;

		memset (valid, 0, sizeof (int) * n);

		int xmin = INT_MAX;
		int ymin = INT_MAX;

		int k;

		for (k = 0; k < n; k++) {

			cin >> xarr[k] >> yarr[k];
			s[k] = xarr[k] + yarr[k];
		}

		for (k = 0; k < n; k++) {

			if (xarr[k] == 0 && yarr[k] == 0) {

				num[k] = number++;
				valid[k] = 1;
				break;
			}
		}

		for (int t = 1; t < n; t++) {

			for (k = 0; k < n; k++) {

				if (valid[k] == 1)
					continue;

				if (xarr[number] == xarr[k])
					xeq[xl++] = k;

				if (yarr[number] == yarr[k])
					yeq[yl++] = k;
			}

			for (k = 0; k < xl; k++) {

				if (valid[k] == 1)
					continue;

				if (xmin > abs (s[num[number]] - s[k])) {

					xmin = abs (s[num[number]] - s[k]);
					xIdx = k;
				}
			}

			for (k = 0; k < yl; k++) {

				if (valid[k] == 1)
					continue;

				if (ymin > abs (s[num[number]] - s[k])) {

					ymin = abs (s[num[number]] - s[k]);
					yIdx = k;
				}
			}

			if (xmin < ymin) {

				num[xIdx] = number++;
				valid[xIdx] = 1;

			}

			else {

				num[yIdx] = number++;
				valid[yIdx] = 1;
			}
		}

		for (i = 0; i < n; i++) {

			cout << num[i] << endl;
		}
	}

	return 0;
}
