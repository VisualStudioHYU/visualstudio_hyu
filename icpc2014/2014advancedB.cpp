/* OX 퀴즈 */

#include <iostream>

using namespace std;

long long arr[91];
int i;

long long fib (int n) {

    arr[1] = arr[2] = 1;
    arr[3] = 2;
    arr[4] = 3;

    for (int i = 5; i <= n; i++) {

        arr[i] = arr[i - 1] + arr[i - 2];
    }

    return arr[n];
}

int main () {

    int C; cin >> C;

    arr[1] = 2;
    arr[2] = 3;

    for (int c = 0; c < C; c++) {

        int n; cin >> n;

        cout << fib (n) << endl;
    }

    return 0;
}

/*
2
3
5

*/
