/* Expensive Tiling */

#include <iostream>

using namespace std;

int main () {

    int T; cin >> T;

    for (int t = 0; t < T; t++) {

        int m, n; cin >> n >> m;

        long long **arr = new long long*[n];

        for (int i = 0; i < n; i++) {

            arr[i] = new long long[m];
        }

        long long sum = 0;

        for (int i = 0; i < n; i++) {

            long long add = i + 1;
            long long temp = 0;

            for (int j = 0; j < m; j++) {

                temp += add;
                arr[n - i - 1][m - j - 1] = temp;
            }
        }

        for (int i = 0; i < n; i++) {

            for (int j = 0; j < m; j++) {

                sum += (i + 1) * (j + 1) * arr[i][j];
            }

            cout << endl;
        }

        cout << sum << endl;
    }

    return 0;
}

/*
1
23

*/