/* 수 합치기 */

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;

int main () {

    int testCase;

    cin >> testCase;

    for (int z = 0; z < testCase; z++) {

        int n; cin >> n;

        vector <int> v;

        for (int y = 0; y < n; y++) {

            int x;
            cin >> x;
            v.push_back (x);
        }

        int sum = 0;

        for (int t = n - 1; t > 0; t--) {

            sort (v.begin (), v.end (), greater <int> ());
            int min1 = v[t];
            int min2 = v[t - 1];
            sum += (min1 + min2);
            v.pop_back ();
            v.pop_back ();
            v.push_back (min1 + min2);
        }

        cout << sum << endl;
    }

    return 0;
}

/*
1
5
4 1 3 2 6

*/