#include <iostream>
#include <set>

using namespace std;

int main(void)
{
	int t;

	cin >> t;

	for (int k = 0; k < t; k++)//전체케이스반복
	{
		int n, m;
		set<int> moon;

		cin >> n >> m;

		for (int i = 0; i < n; i++)
		{
			int student;
			int sol;

			cin >> student;

			if (student < m / 2)
			{
				m = 53;
			}

			for (int j = 0; j < student; j++)
			{
				cin >> sol;
				moon.insert(sol);
			}
		}

		int answer = moon.size();

		if (answer < m)
		{
			cout << "Let's study" << endl;
		}
		else
		{
			cout << "Let's drink" << endl;
		}
	}


	return 0;
}