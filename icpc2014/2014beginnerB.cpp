﻿/* 파스칼 직각삼각형 */

#include <iostream>
#include <stdlib.h>

using namespace std;

void pascal (int count) {

    long long **arr;

    arr = (long long **)malloc (sizeof (long long *) * count);

    arr[0] = (long long*)malloc (sizeof (long long) * 3);
    arr[0][0] = arr[0][2] = 0;
    arr[0][1] = 1;

    for (int i = 1; i < count; i++) {

        arr[i] = (long long *)malloc (sizeof (long long) * (i + 3));
        arr[i][0] = arr[i][i + 2] = 0;

        for (int j = 1; j < i + 2; j++) {

            arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
        }
    }

    for (int i = 0; i < count; i++) {

        for (int j = 1; j < i + 2; j++) {

            cout << arr[i][j] << " ";
        }

        cout << endl;
    }
}

int main () {


    int n;
    int i;
    int *arr;
    int m;

    cin >> n;

    for (i = 0; i < n; i++) {

        cin >> m;
        pascal (m);
    }

    return 0;
}

/*
3
1
2
3

*/
