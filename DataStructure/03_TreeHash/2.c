/* 2015-01-20
 * Binary tree
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TreeNode {
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode *tree_root;

int FindNode(struct TreeNode *root, int data);
int InsertNode(struct TreeNode *root, int data);
int PrintPreorder(struct TreeNode *root);
int PrintInorder(struct TreeNode *root);
int PrintPostorder(struct TreeNode *root);

int main() {
    char command[5];
    int data;

    tree_root = NULL;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "i")) {
            scanf("%d", &data);
            if (InsertNode(tree_root, data) == -1) {
                printf("%d can't insert\n", data);
            }
        }
        else if (!strcmp(command, "f")) {
            scanf("%d", &data);
            if (FindNode(tree_root, data) == -1) {
                printf("%d does not exist\n", data);
            }
            else {
                printf("%d does exist\n", data);
            }
        }
        else if (!strcmp(command, "pre")) {
            if (PrintPreorder(tree_root) == -1) {
                puts("Tree does not exist");
            }
            puts("");
        }
        else if (!strcmp(command, "in")) {
            if (PrintInorder(tree_root) == -1) {
                puts("Tree does not exist");
            }
            puts("");
        }
        else if (!strcmp(command, "post")) {
            if (PrintPostorder(tree_root) == -1) {
                puts("Tree does not exist");
            }
            puts("");
        }
    }
    return 0;
}

int FindNode(struct TreeNode *root, int data) {
    if (tree_root == NULL) {
        return -1;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        if (root->data == data) {
            return 0;
        }
    }
    else if (root->left != NULL) {
        if (FindNode(root->left, data) == 0) {
            return 0;
        }
        if (root->data == data) {
            return 0;
        }
        if (FindNode(root->right, data) == 0) {
            return 0;
        }
    }
    else if (root->right != NULL) {
        if (FindNode(root->right, data) == 0) {
            return 0;
        }
        if (root->data == data) {
            return 0;
        }
    }
    return -1;
}

int InsertNode(struct TreeNode *root, int data) {
    /* 트리에 처음 노드를 추가 */
    if (tree_root == NULL) {
        struct TreeNode *new_node = (struct TreeNode *) malloc(
            sizeof(struct TreeNode));
        new_node->data = data;
        new_node->left = new_node->right = NULL;
        tree_root = new_node;
        return 0;
    }

    /* 데이터를 왼쪽 자식 노드에 추가하는 경우 */
    if (data < root->data) {
        /* 왼쪽으로 이동 */
        if (root->left != NULL) {
            InsertNode(root->left, data);
        }
        else {
            struct TreeNode *new_node = (struct TreeNode *) malloc(
                sizeof(struct TreeNode));
            new_node->left = new_node->right = NULL;
            new_node->data = data;
            root->left = new_node;
        }
    }
    /* 데이터를 오른쪽 자식 노드에 추가하는 경우 */
    else if (data > root->data) {
        /* 오른쪽으로 이동 */
        if (root->right != NULL) {
            InsertNode(root->right, data);
        }
        else {
            struct TreeNode *new_node = (struct TreeNode *) malloc(
                sizeof(struct TreeNode));
            new_node->left = new_node->right = NULL;
            new_node->data = data;
            root->right = new_node;
        }
    }
    else {
        printf("%d already exsit\n", data);
    }
    return 0;
}

int PrintPreorder(struct TreeNode *root) {
    if (tree_root == NULL) {
        return -1;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        printf("%d ", root->data);
    }
    else if (root->left != NULL) {
        printf("%d ", root->data);
        PrintPreorder(root->left);
        PrintPreorder(root->right);
    }
    else if (root->right != NULL) {
        printf("%d ", root->data);
        PrintPreorder(root->right);
    }
    return 0;
}

int PrintInorder(struct TreeNode *root) {
    if (tree_root == NULL) {
        return -1;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        printf("%d ", root->data);
    }
    else if (root->left != NULL) {
        PrintInorder(root->left);
        printf("%d ", root->data);
        PrintInorder(root->right);
    }
    else if (root->right != NULL) {
        printf("%d ", root->data);
        PrintInorder(root->right);
    }
    return 0;
}

int PrintPostorder(struct TreeNode *root) {
    if (tree_root == NULL) {
        return -1;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        printf("%d ", root->data);
    }
    else if (root->left != NULL) {
        PrintPostorder(root->left);
        PrintPostorder(root->right);
        printf("%d ", root->data);
    }
    else if (root->right != NULL) {
        PrintPostorder(root->right);
        printf("%d ", root->data);
    }
    return 0;
}
