/* 2015-01-16
 * Binary tree
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

/* Structure for tree */
struct TreeNode {
    struct TreeNode *left;
    int data;
    struct TreeNode *right;
    int position;
};

/* Global variable(root, cursor) */
/* Root node for tree */
struct TreeNode *tree_root;
/* Cursor for tree travel */
struct TreeNode *cursor;

/* Global variable for tree node position */
int global_position;

void InsertNode(struct TreeNode *root, int data);
void DeleteNode(struct TreeNode *root, int data);
void PrintInorder(struct TreeNode *root);

int main() {
    tree_root = cursor = NULL;
    global_position = 1;
    char cmd;

    while (1) {
        scanf("%c", &cmd);
        int data;

        if (cmd == 'i') {
            scanf("%d", &data);
            InsertNode(tree_root, data);
        }
        else if (cmd == 'd') {
            scanf("%d", &data);
            DeleteNode(tree_root, data);
        }
        else if (cmd == 'p') {
            PrintInorder(tree_root);
        }
    }
    return 0;
}

void InsertNode(struct TreeNode *root, int data) {
    /* 맨 처음 노드 삽입 */
    if (tree_root == NULL) {
        struct TreeNode *new_node = (struct TreeNode *) malloc(
            sizeof(struct TreeNode));
        new_node->data = data;
        new_node->left = new_node->right = NULL;
        new_node->position = global_position++;
        tree_root = new_node;
        return;
    }

    /* 데이터가 0이라면  */
    if (root->data == 0) {
        root->data = data;
        return;
    }

    /* 데이터가 왼쪽 자식에 추가될 경우 */
    if (data < root->data) {
        if (root->left != NULL) {
            InsertNode(root->left, data);
        }
        else {
            if (root->data == 0) {
                root->data = data;
                return;
            }
            struct TreeNode *new_node = (struct TreeNode *) malloc(
                sizeof(struct TreeNode));
            new_node->left = new_node->right = NULL;
            new_node->data = data;
            new_node->position = global_position++;
            root->left = new_node;
        }
    }
    /* 데이터가 오른쪽 자식에 추가될 경우 */
    else if (data > root->data) {
        if (root->right != NULL) {
            InsertNode(root->right, data);
        }
        else {
            if (root->data == 0) {
                root->data = data;
                return;
            }
            struct TreeNode *new_node = (struct TreeNode *) malloc(
                sizeof(struct TreeNode));
            new_node->left = new_node->right = NULL;
            new_node->data = data;
            new_node->position = global_position++;
            root->right = new_node;
        }
    }
    return;
}

void DeleteNode(struct TreeNode *root, int data) {
    /* 아직 트리가 생성되지 않음 */
    if (tree_root == NULL) {
        puts("Tree does not exist");
        return;
    }

    /* 루트가 없을 경우 종료 */
    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        if (root->data == data) {
            root->data = 0;
            printf("%d deleted\n", data);
            return;
        }
    }
    else if (root->left != NULL) {
        DeleteNode(root->left, data);
        if (root->data == data) {
            root->data = 0;
            printf("%d deleted\n", data);
            return;
        }
        DeleteNode(root->right, data);
    }
    else if (root->right != NULL) {
        if (root->data == data) {
            root->data = 0;
            printf("%d deleted\n", data);
            return;
        }
        DeleteNode(root->right, data);
    }
    return;
}

void PrintInorder(struct TreeNode *root) {
    if (tree_root == NULL) {
        return;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        if (root->data != 0) {
            printf("%d: %d\n", root->position, root->data);
        }
    }
    else if (root->left != NULL) {
        PrintInorder(root->left);
        if (root->data != 0) {
            printf("%d: %d\n", root->position, root->data);
        }
        PrintInorder(root->right);
    }
    else if (root->right != NULL) {
        if (root->data != 0) {
            printf("%d: %d\n", root->position, root->data);
        }
        PrintInorder(root->right);
    }
    return;
}
