/* 2015-01-21
 * Priority queue (Min heap based array)
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

/* Max heap size */
#define HEAP_CAPACITY 20
/* Because priority queue */
typedef int priority;
/* Heap based array */
priority arr[HEAP_CAPACITY];

int global_heap_size;

int Insert(priority pri);
int Delete(priority pri);
int Print();

int main() {
    char command[2];
    priority pri;

    memset((void *)arr, INT_MAX, HEAP_CAPACITY);
    global_heap_size = 0;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "i")) {
            scanf("%d", &pri);
            if (Insert(pri) == -1) {
                printf("%d can't insert\n", pri);
            }
        }
        else if (!strcmp(command, "d")) {
            if (Delete(pri) == -1) {
                printf("%d can't delete\n", pri);
            }
        }
        else if (!strcmp(command, "p")) {
            if (Print() == -1) {
                puts("Heap is empty");
            }
        }
    }
    return 0;
}

int Insert(priority pri) {
    /* 힙이 최대일 경우 */
    if (global_heap_size == 19) {
        return -1;
    }

    int cursor = ++global_heap_size;
    int parent = cursor / 2;
    
    arr[cursor] = pri;
    
    /* 트리를 배열로 만들 경우
     * 부모 노드의 번호를 n이라 하면,
     * 왼쪽 자식 노드는 2n, 오른쪽 자식 노드는 2n + 1임 */
    while (1) {
        if (parent == 0) {
            break;
        }

        if (arr[parent] > arr[cursor]) {
            priority tmp = arr[parent];
            arr[parent] = arr[cursor];
            arr[cursor] = tmp;
        }
        parent /= 2;
        cursor /= 2;
    }
    return 0;
}

int Delete(priority pri) {
    /* 힙에 아무것도 들어있지 않은 경우 */
    if (arr[1] == INT_MAX) {
        return -1;
    }

    int cursor = 1;
    int ret = arr[cursor];

    /* 마지막 노드를 맨 위로 올림 */
    arr[cursor] = arr[global_heap_size--];

    /* 맨 위로 올렸던 노드의 위치를 찾아감 */
    while (1) {
        int left_child = cursor * 2;
        int right_child  = left_child + 1;

        /* 노드가 적절한 위치를 찾은 경우 종료 */
        if (left_child >= global_heap_size) {
            break;
        }

        if (arr[left_child] < arr[right_child]) {
            priority tmp = arr[cursor];
            arr[cursor] = arr[left_child];
            arr[left_child] = tmp;
            cursor = left_child;
        }
        else {
            priority tmp = arr[cursor];
            arr[cursor] = arr[right_child];
            arr[right_child] = tmp;
            cursor = right_child;
        }
    }
}

int Print() {
    if (global_heap_size == 0) {
        return -1;
    }

    int i;
    for (i = 1; i < global_heap_size + 1; i++) {
        printf("%d: %d", i, arr[i]);

        if (i != global_heap_size) {
            printf(", ");
        }
    }

    puts("");
    return 0;
}
