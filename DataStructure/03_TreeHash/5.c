/* 2015-01-21
 * Hash table
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define TABLE_SIZE 100

enum State {
    EMPTY, USING, DELETED
};

struct Node {
    char *name;
    int height;
    int weight;
    enum State state;
};

struct Node hash_table[TABLE_SIZE];
int size;

int TableInit();
int InsertNode(char *name, int height, int weight);
int Hashing(char *key);
int FindNode(char *name);
int DeleteNode(char *name);
int Print();

int main() {
    char command[2];
    
    size = 0;

    if (TableInit() == -1) {
        puts("table init error");
        return 0;
    }

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "i")) {
            char *name = (char *) malloc(sizeof(char) * 20);
            int height;
            int weight;

            scanf("%s", name);
            scanf("%d", &height);
            scanf("%d", &weight);
            
            if (InsertNode(name, height, weight) == -1) {
                printf("%s can't insert\n", name);
            }
        }
        else if (!strcmp(command, "f")) {
            char *name = (char *) malloc(sizeof(char) * 20);
            scanf("%s", name);

            if (FindNode(name) == -1) {
                printf("%s does not exist\n", name);
            }
        }
        else if (!strcmp(command, "d")) {
            char *name = (char *) malloc(sizeof(char) * 20);
            scanf("%s", name);

            if (DeleteNode(name) == -1) {
                printf("%s can't delete\n", name);
            }
            else {
                printf("%s is deleted\n", name);
            }
        }
        else if (!strcmp(command, "p")) {
            if (Print() == -1) {
                puts("hash table is empty");
            }
        }
    }
    return 0;
}


/* 테이블 초기화 */
int TableInit() {
    if (size != 0) {
        return -1;
    }

    int i;

    for (i = 0; i < TABLE_SIZE; i++) {
        hash_table[i].state = EMPTY;
    }

    return 0;
}

int InsertNode(char *name, int height, int weight) {
    int key = Hashing(name);
    int collision = 0;
    int i;

    for (i = 0; i < 3; i++) {
        if (hash_table[key].state == USING) {
            collision++;
            key += pow(collision, 2);
            if (key >= 100) {
                return -1;
            }
            puts("collision");
        }
        else {
            break;
        }
    }

    if (collision == 3) {
        return -1;
    }
    /* 적절한 위치를 찾은 경우 삽입 */
    else {
        hash_table[key].name = (char *) malloc(sizeof(char) * 20);
        if (strcpy(hash_table[key].name, name) == NULL) {
            puts("input error");
            return -1;
        }
        hash_table[key].height = height;
        hash_table[key].weight = weight;
        hash_table[key].state = USING;
        printf("%s is located %d\n", hash_table[key].name, key);
        size++;
        return 0;
    }
    return 0;
}

/* 해시 알고리즘 */
int Hashing(char *key) {
    int i;
    int ret = 0;

    for (i = 0; i < strlen(key); i++) {
        ret += (int)key[i];
    }
   
    ret %= 100;
    return ret;
}

int FindNode(char *name) {
    int key = Hashing(name);
    int i;

    for (i = 0; i < 3; i++) {
        if (hash_table[key].state == USING) {
            if(!strcmp(hash_table[key].name, name)) {
                printf("%s is located %d, h %d, w %d\n",
                    hash_table[key].name, key,
                    hash_table[key].height, hash_table[key].weight);
                return 0;
            }
        }
    }
    return -1;
}

int DeleteNode(char *name) {
    int key = Hashing(name);
    int i;

    for (i = 0; i < 3; i++) {
        if (hash_table[key].state == USING) {
            if(!strcmp(hash_table[key].name, name)) {
                hash_table[key].state = DELETED;
                size--;
                return 0;
            }
        }
    }
    return -1;
}

int Print() {
    if (size == 0) {
        return -1;
    }
    int i;

    for (i = 0; i < TABLE_SIZE; i++) {
        if (hash_table[i].state == USING) {
            printf("%s is located %d, h %d, w %d\n",
                    hash_table[i].name, i,
                    hash_table[i].height, hash_table[i].weight);
        }
    }
    return 0;
}
