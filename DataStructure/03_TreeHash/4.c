/* 2015-01-22
 * AVL Tree
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TreeNode {
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode *tree_root;

int GetHeight(struct TreeNode *root);
struct TreeNode* InsertNode(struct TreeNode **root, int data);
int PrintInorder(struct TreeNode *root);
int GetBalance(struct TreeNode *root);
struct TreeNode* MakeBalance(struct TreeNode **root);
struct TreeNode* RotateLL(struct TreeNode *parent);
struct TreeNode* RotateRR(struct TreeNode *parent);
struct TreeNode* RotateLR(struct TreeNode *parent);
struct TreeNode* RotateRL(struct TreeNode *parent);

int main() {
    char command[2];
    int data;

    tree_root = NULL;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "i")) {
            scanf("%d", &data);
            if (InsertNode(&tree_root, data) == NULL) {
                printf("%d can't insert\n", data);
            }
        }
        else if (!strcmp(command, "h")) {
            int tree_height = GetHeight(tree_root);

            if (tree_height == -1) {
                puts("Tree does not exist");
            }
            else {
                printf("%d\n", tree_height + 1);
            }
        }
        else if (!strcmp(command, "p")) {
            if (PrintInorder(tree_root) == -1) {
                puts("Tree does not exist");
            }
            puts("");
        }
        else if (!strcmp(command, "q")) {
            break;
        }
    }
    return 0;
}

int GetBalance(struct TreeNode *root) {
    /* 트리가 아직 생성되지 않은 경우 */
    if (root == NULL) {
        return -1;
    }

    int ret;
    ret = GetHeight(root->left) - GetHeight(root->right);

    return ret;
}

int GetHeight(struct TreeNode *root) {
    if (root == NULL) {
        return -1;
    }

    int left_height;
    int right_height;

    /* 좌우 높이를 찾아 차이를 반환 */
    right_height = GetHeight(root->right);
    left_height = GetHeight(root->left);

    if (left_height > right_height) {
        return left_height + 1;
    }
    return right_height + 1; 
}

struct TreeNode* InsertNode(struct TreeNode **root, int data) {
    if (*root == NULL) {
        *root = (struct TreeNode *) malloc(sizeof(struct TreeNode));
        (*root)->data = data;
        (*root)->left = (*root)->right = NULL;
    }
    else if (data < (*root)->data) {
        (*root)->left = InsertNode(&((*root)->left), data);
        (*root) = MakeBalance(root);
    }
    else if (data > (*root)->data) {
        (*root)->right = InsertNode(&((*root)->right), data);
        (*root) = MakeBalance(root);
    }
    else {
        printf("%d already exsit\n", data);
    }
    return *root;
}

int PrintInorder(struct TreeNode *root) {
    if (tree_root == NULL) {
        return -1;
    }

    if (root == NULL) {
    }
    else if (root->left == NULL && root->right == NULL) {
        printf("%d ", root->data);
    }
    else if (root->left != NULL) {
        PrintInorder(root->left);
        printf("%d ", root->data);
        PrintInorder(root->right);
    }
    else if (root->right != NULL) {
        printf("%d ", root->data);
        PrintInorder(root->right);
    }
    return 0;
}

struct TreeNode* MakeBalance(struct TreeNode **root) {
    int height_gap = GetBalance(*root);

    /* 좌우 높이가 2 이상 차이나는 경우
     * 적절한 회전을 통해 균형을 맞춤 */
    if (height_gap > 1) {
        if (GetBalance((*root)->left) > 0) {
            *root = RotateLL(*root);
        }
        else {
            *root = RotateLR(*root);
        }
    }
    else if (height_gap < -1) {
        if (GetBalance((*root)->right) < 0) {
            *root = RotateRR(*root);
        }
        else {
            *root = RotateRL(*root);
        }
    }

    return *root;
}

struct TreeNode* RotateLL(struct TreeNode *parent) {
    struct TreeNode *child = parent->left;
    parent->left = child->right;
    child->right = parent;

    return child;
}

struct TreeNode* RotateRR(struct TreeNode *parent) {
    struct TreeNode *child = parent->right;
    parent->right = child->left;
    child->left = parent;

    return child;
} 

/* RL회전은 부분적인 LL회전 후 RR 회전을 하는 것과 같음 */
struct TreeNode* RotateRL(struct TreeNode *parent) {

    struct TreeNode *child = parent->right;
    parent->right = RotateLL(child);

    return RotateRR(parent);
} 

/* LR회전은 부분적인 RR회전 후 LL 회전을 하는 것과 같음 */
struct TreeNode* RotateLR(struct TreeNode *parent) {

    struct TreeNode *child = parent->left;
    parent->left = RotateRR(child);

    return RotateLL(parent);
} 
