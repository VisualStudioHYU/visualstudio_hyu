/* 2015-01-14
 * Circular queue based array
 * Write by Yedarm Seong */

#include <stdio.h>
#include <limits.h>

#define QUEUE_SIZE 5

void Enqueue(int queue[], int x);
int Dequeue(int queue[]);

int front, rear;
int queue_size;

int main() {
    int queue[QUEUE_SIZE];
    char command[2];
    int data;
    
    front = rear = 0;
    queue_size = 0;

    while (1) {
        scanf("%s", command);

        if (command[0] == 'e') {
            scanf("%d", &data);
            Enqueue(queue, data);
        }
        else if (command[0] == 'd') {
            /* Empty is return 0, not empty is return popped data*/
            int not_empty = Dequeue(queue);
            
            if (not_empty) {
                printf("%d is dequeued\n", not_empty);
            }
        }
    }
    return 0;
}

void Enqueue(int queue[], int x) {
    if (queue_size == QUEUE_SIZE) {
        puts("queue is full");
        return;
    }
    
    /* 큐에 삽입을 하고, rear의 위치를 한 칸 뒤로 옮김 */
    queue[(front + queue_size) % QUEUE_SIZE] = x;
    queue_size++;
    rear++;
}

int Dequeue(int queue[]) {
    if (queue_size == 0) {
        puts("queue is empty");
        return 0;
    }

    /* 맨 앞의 원소를 제거하고 빈 자리는 INT_MAX로 채움 */
    int tmp = queue[front];
    queue[front] = INT_MIN;
    queue_size--;
    front++;

    /* 원형 큐이므로 큐의 용량으로 나누어 주어야 원형 큐를 유지함 */
    if (front >= QUEUE_SIZE) {
        front %= QUEUE_SIZE;
    }

    return tmp;
}
