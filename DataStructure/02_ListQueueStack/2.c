/* 2015-01-14
 * Stack based array
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define STACK_SIZE 5

int top;

void push(int arr[], int x);
int pop(int arr[]);

int main() {
    int arr[STACK_SIZE];
    int data;
    char command[5];

    top = -1;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "push")) {
            scanf("%d", &data);
            push(arr, data);
        }
        else if (!strcmp(command, "pop")) {
            /*  */
            int not_empty = pop(arr);
            
            if (not_empty) {
                printf("%d is popped\n", not_empty);
            }
        }
    }
    return 0;
}

void push(int arr[], int x) {
    /* 스택이 다 찼다면 더 추가하지 않고 다 찬 상태라고 알려줌 */
    if (top == 4) {
        puts("stack is full");
        return;
    }

    arr[++top] = x;
}

int pop(int arr[]) {
    /* 더 꺼낼 원소가 없다면 비어있다고 알려줌 */
    if (top == -1) {
        puts("stack is empty");
        return 0;
    }

    int tmp = arr[top];
    arr[top--] = INT_MIN;

    return tmp;
}
