/* 2015-01-14
 * Doubly linked list
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

struct Node {
    int data;
    struct Node *prev;
    struct Node *next;
};

/* 노드 삽입 제거의 편의를 위한 더미 노드 */
struct Node *dummy_head;

void InsertNode(int x);
void Print();
void Delete(int x);
void InsertLeftNode(int x, int y);
void InsertRightNode(int x, int y);

int main() {
    struct Node *tmp = NULL;
    char command[4];
    int x, y;
    
    /* 더미 헤드 초기화 */
    dummy_head = (struct Node *) malloc(sizeof(struct Node));
    dummy_head->prev = dummy_head->next = NULL;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "il")) {
            scanf("%d %d", &x, &y);
            InsertLeftNode(x, y);
        }
        else if (!strcmp(command, "ir")) {
            scanf("%d %d", &x, &y);
            InsertRightNode(x, y);
        }
        else if (command[0] == 'i') {
            scanf("%d", &x);
            InsertNode(x);
        }
        else if (command[0] == 'p') {
            Print();
        }
        else if (command[0] == 'd') {
            scanf("%d", &x);
            Delete(x);
        }
    }
    return 0;
}

void InsertLeftNode(int x, int y) {
    struct Node *cursor = dummy_head;

    while (1) {
        /* 타겟 x를 찾으면, 그 왼쪽에 삽입 */
        if (cursor->data == x) {
            struct Node *new_node = (struct Node *) malloc(
                sizeof(struct Node));
            new_node->data = y;
            new_node->prev = cursor->prev;
            new_node->next = cursor;
            cursor->prev->next = new_node;
            cursor->prev = new_node;
            return;
        }
        /* 타겟 자체가 없다면 종료 */
        else if (cursor->next == NULL) {
            printf("%d does not exsit\n", x);
            return;
        }
        /* 타겟을 찾을때까지 다음 노드로 이동 */
        cursor = cursor->next;
    }
}

void InsertRightNode(int x, int y) {
    struct Node *cursor = dummy_head;

    while (1) {
        if (cursor->data == x) {
            struct Node *new_node = (struct Node *) malloc(
                sizeof(struct Node));
            new_node->data = y;
            new_node->prev = cursor;
            new_node->next = cursor->next;

            if (cursor->next != NULL) {
                cursor->next->prev = new_node;
            }

            cursor->next = new_node;
            return;
        }
        else if (cursor->next == NULL) {
            printf("%d does not exsit\n", x);
            return;
        }
        cursor = cursor->next;
    }
}

void InsertNode(int x) {
    struct Node *cursor = dummy_head;

    /* 마지막 위치를 찾아 이동 */
    while (1) {
        if (cursor->next == NULL) {
            break;
        }
        cursor = cursor->next;
    }

    /* 마지막 위치에 노드 추가 */
    struct Node *new_node = (struct Node *) malloc(sizeof(struct Node));
    cursor->next = new_node;
    new_node->data = x;
    new_node->next = NULL;
    new_node->prev = cursor;
}

void Print() {
    struct Node *cursor = dummy_head->next;
    
    if (cursor == NULL) {
        return;
    }

    while (1) {
        printf("%d ", cursor->data);
        
        if (cursor->next == NULL) {
            puts("-|");
            break;
        }
        
        printf("->");
        cursor = cursor->next;
    }
}

void Delete(int x) {
    struct Node *cursor = dummy_head;
    struct Node *tmp = NULL;

    /* 쓰레기 노드부터 시작하여 타겟이 나올 때까지 검색 */
    while (1) {
        if (cursor->data == x) {
            tmp = cursor;
            cursor->prev->next = cursor->next;

            if (cursor->next != NULL) {
                cursor->next->prev = cursor->prev;
            }

            free(tmp);
            return;
        }
        else if (cursor->next == NULL) {
            printf("%d does not exsit\n", x);
            return;
        }
        cursor = cursor->next;
    }
}
