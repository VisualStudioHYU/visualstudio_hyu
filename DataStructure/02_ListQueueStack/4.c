/* 2015-01-14
 * Queue based linked list
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

struct Queue {
    int data;
    struct Queue *next;
};

void Enqueue(struct Queue *queue, int x);
int Dequeue(struct Queue *queue);

int main() {
    /* 더미 노드와 같은 역할 */
    struct Queue *queue_list = (struct Queue *) malloc(
        sizeof(struct Queue));
    int data;
    char command[2];

    queue_list->next = NULL;

    while (1) {
        scanf("%s", command);

        if (command[0] == 'e') {
            scanf("%d", &data);
            Enqueue(queue_list, data);
        }
        else if (command[0] == 'd') {
            /* Empty is return 0, not empty is return popped data*/
            int not_empty = Dequeue(queue_list);
            
            if (not_empty) {
                printf("%d is dequeued\n", not_empty);
            }
        }
    }
    return 0;
}

void Enqueue(struct Queue *queue, int x) {
    struct Queue *new_queue = (struct Queue *) malloc(
        sizeof(struct Queue));
    new_queue->data = x;
    new_queue->next = NULL;

    struct Queue *cursor = queue;

    while (1) {
        if (cursor->next == NULL) {
            cursor->next = new_queue;
            break;
        }
        cursor = cursor->next;
    }
}

int Dequeue(struct Queue *queue) {
    if (queue->next == NULL) {
        puts("queue is empty");
        return 0;
    }

    struct Queue *cursor = queue->next;
    int tmp = cursor->data;
    queue->next = cursor->next;
    free(cursor);

    return tmp;
}
