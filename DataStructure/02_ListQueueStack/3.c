/* 2015-01-14
 * Stack based linked list
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

struct Stack {
    int data;
    struct Stack *next;
};

int top;

void push(struct Stack *stack, int x);
int pop(struct Stack *stack);

int main() {
    /* 더미 노드와 같은 역할 */
    struct Stack *stack_list = (struct Stack *) malloc(
        sizeof(struct Stack));
    int data;
    char command[5];

    top = -1;
    stack_list->next = NULL;

    while (1) {
        scanf("%s", command);

        if (!strcmp(command, "push")) {
            scanf("%d", &data);
            push(stack_list, data);
        }
        else if (!strcmp(command, "pop")) {
            /* Empty is return 0, not empty is return popped data*/
            int not_empty = pop(stack_list);
            
            if (not_empty) {
                printf("%d is popped\n", not_empty);
            }
        }
    }
    return 0;
}

void push(struct Stack *stack, int x) {
    /* 새 노드를 생성 */
    struct Stack *new_stack = (struct Stack *) malloc(
        sizeof(struct Stack));
    new_stack->data = x;
    new_stack->next = NULL;

    struct Stack *cursor = stack;

    while (1) {
        /* 마지막 위치에 삽입함 */
        if (cursor->next == NULL) {
            cursor->next = new_stack;
            break;
        }
        cursor = cursor->next;
    }
    top++;
}

int pop(struct Stack *stack) {
    if (stack->next == NULL) {
        puts("stack is empty");
        return 0;
    }

    /* 꺼내기 전 top과 꺼낸 후의 top의 위치를 저장하기 위한 포인터 */
    struct Stack *cursor = stack;
    struct Stack *previous_cursor = stack;

    while (1) {
        if (cursor->next == NULL) {
            break;
        }
        previous_cursor = cursor;
        cursor = cursor->next;
    }

    /* top의 위치를 한 칸 앞으로 이동 */
    int tmp = cursor->data;
    previous_cursor->next = NULL;
    free(cursor);
    top--;

    return tmp;
}
