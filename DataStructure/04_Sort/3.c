/* 2015-01-28
 * Quick sort
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 101

void Swap(int arr[], int x, int y);
int Partition(int arr[], int left, int right);
int QuickSort(int arr[], int left, int right);

int main() {
    int n;
    int i;
    int arr[ARRAY_SIZE];

    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    if (QuickSort(arr, 0, n - 1) == 0) {
        puts("sort error");
        return 0;
    }

    for (i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }

    puts("");
    return 0;
}

void Swap(int arr[], int x, int y) {
    int temp = arr[x];
    arr[x] = arr[y];
    arr[y] = temp;
}

int Partition(int arr[], int left, int right) {
    int pivot = arr[right];
    int low = left;
    int high = right - 1;
    int i;

    while (1) {
        if (low > high) {
            break;
        }

        /* low가 자기 위치를 찾아감 */
        while (1) {
            if (pivot < arr[low]) {
                break;
            }
            else if (low > right - 1) {
                break;
            }
            low++;
        }

        /* high가 자기 위치를 찾아감 */
        while (1) {
            if (pivot > arr[high]) {
                break;
            }
            else if (high < left) {
                break;
            }
            high--;
        }

        /* low와 high를 비교하여 위치를 바꿈 */
        if (low < high + 1) {
            Swap(arr, low, high);
        }
    }

    /* low와 high가 교차되면, low와 pivot을 교환 */
    Swap(arr, right, low);
    return low;
}

int QuickSort(int arr[], int left, int right) {
    if (arr == NULL) {
        return 0;
    }

    if (left < right + 1) {
        /* 피벗을 기준으로 좌측 공간과 우측 공간으로 나뉘어짐 */
        int pivot = Partition(arr, left, right);
        int i;
        for (i = 0; i < right + 1; i++) {
            printf("%d ", arr[i]);
        }
        puts("");
        /* 피벗은 이미 적당한 위치이므로 정렬 대상에 포함하지 않음 */
        QuickSort(arr, left, pivot - 1);
        QuickSort(arr, pivot + 1, right);
    }

    return 1;
}
