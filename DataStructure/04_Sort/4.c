/* 2015-01-28
 * Radix sort
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRAY_SIZE 101
#define NUMBER_SIZE 3

struct Node {
    char data[NUMBER_SIZE + 1];
    struct Node *next;
};

/* int형 숫자를 char *형식으로 변환하여
 * 한 문자씩 비교하며 정렬 */
int RadixSort(char arr[][NUMBER_SIZE + 1], struct Node *chain_arr[],
                 int radix, int size);
int Insert(char data[], struct Node *chain_arr[], int index);

int main() {
    int n;
    int i;
    struct Node *chain_arr[10];

    char *input_tmp = (char *) malloc(sizeof(char) * (NUMBER_SIZE + 1));
    char arr[ARRAY_SIZE][NUMBER_SIZE + 1];

    for (i = 0 ; i < 10; i++) {
        chain_arr[i] = (struct Node *) malloc(sizeof(struct Node));
        strcpy(chain_arr[i]->data, "-1");
        chain_arr[i]->next = NULL;
    }

    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        scanf("%s", input_tmp);
        sprintf(arr[i], "%03s%c", input_tmp, NULL);    
    }

    for (i = 1; i < NUMBER_SIZE + 1; i++) {
        RadixSort(arr, chain_arr, i, n);
    }

    for (i = 0; i < n; i++) {
        printf("%s ", arr[i]);
    }

    return 0;
}

int RadixSort(char arr[][NUMBER_SIZE + 1], struct Node *chain_arr[],
                int radix, int size) {
    int i;
    int rad = NUMBER_SIZE - radix;
    int index;
    int j = 0;

    for (i = 0; i < size; i++) {
        index = ((int)arr[i][rad] - 48);
        Insert(arr[i], chain_arr, index);
    }

    for (i = 0; i < 10; i++) {
        struct Node *cursor = chain_arr[i];

        while (1) {
            if (cursor == NULL) {
                break;
            }
            else if (strcmp(cursor->data, "-1")) {
                strcpy(arr[j++], cursor->data);
            }
            cursor = cursor->next;
        }
    }

    for (i = 0; i < 10; i++) {
        struct Node *cursor = chain_arr[i];

        if (cursor->next == NULL) {
            continue;
        }

        while (1) {
            while (1) {
                if (cursor->next->next == NULL) {
                    break;
                }
                cursor = cursor->next;
            }

            free(cursor->next);
            cursor->next = NULL;
            cursor = chain_arr[i];

            if (cursor->next == NULL) {
                break;
            }
        }
    }

    return 1;
}

int Insert(char data[], struct Node *chain_arr[], int index) {
    if (data == NULL) {
        return 0;
    }
    else if (chain_arr == NULL) {
        return 0;
    }
    else if (index < 0) {
        return 0;
    }
    else if (index > 9) {
        return 0;
    }

    struct Node *cursor = chain_arr[index];

    while (1) {
        if (cursor->next == NULL) {
            struct Node *new_node =
                (struct Node *) malloc(sizeof(struct Node));
            strcpy(new_node->data, data);
            new_node->next = NULL;
            cursor->next = new_node;
            return 1;
        }
        cursor = cursor->next;
    }
}
