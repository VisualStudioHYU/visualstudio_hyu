/* 2015-01-24
 * Heap sort
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

/* Max heap size */
#define HEAP_SIZE 101
/* Because priority queue */
typedef int priority;
/* Heap based array */
priority arr[HEAP_SIZE];

int global_heap_size;

int HeapSort(int n);
int Insert(priority pri);
int Delete(priority pri);
int Print(int n);

int main() {
    int n;
    int i;
    priority pri;

    memset((void *)arr, INT_MAX, HEAP_SIZE);
    global_heap_size = 0;

    scanf("%d", &n);
    HeapSort(n);
    Print(n);
    return 0;
}

/* 값이 아닌 우선순위를 기반으로 힙을 생성 */
int Insert(priority pri) {
    if (global_heap_size == 100) {
        return -1;
    }

    int cursor = ++global_heap_size;
    int parent = cursor / 2;
    
    arr[cursor] = pri;
    
    while (1) {
        if (parent == 0) {
            break;
        }

        if (arr[parent] > arr[cursor]) {
            priority tmp = arr[parent];
            arr[parent] = arr[cursor];
            arr[cursor] = tmp;
        }
        parent /= 2;
        cursor /= 2;
    }
    return 0;
}

int HeapSort(int n) {
    int i;
    for (i = 1; i < n + 1; i++) {
        scanf("%d", &arr[i]);
        Insert(arr[i]);
    }
    for (i = 1; i < n + 1; i++) {
        Delete(arr[i]);
    }
}

int Delete(priority pri) {
    if (arr[1] == INT_MAX) {
        return -1;
    }

    int cursor = 1;
    int ret = arr[cursor];
    int tmp;

    tmp = arr[cursor];
    arr[cursor] = arr[global_heap_size];
    arr[global_heap_size] = tmp;
    global_heap_size--;

    while (1) {
        int left_child = cursor * 2;
        int right_child  = left_child + 1;

        if (left_child >= global_heap_size) {
            break;
        }

        if (arr[left_child] < arr[right_child]) {
            priority tmp = arr[cursor];
            arr[cursor] = arr[left_child];
            arr[left_child] = tmp;
            cursor = left_child;
        }
        else {
            priority tmp = arr[cursor];
            arr[cursor] = arr[right_child];
            arr[right_child] = tmp;
            cursor = right_child;
        }
    }
    return 0;
}

int Print(int n) {
    int i;
    for (i = 1; i < n + 1; i++) {
        printf("%d ", arr[i]);
    }

    puts("");
    return 0;
}
