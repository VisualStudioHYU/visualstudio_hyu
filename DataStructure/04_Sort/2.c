/* 2015-01-28
 * Merge sort
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 101

int Merge(int *arr, int left, int mid, int right);
int MergeSort(int *arr, int left, int right);

int main() {
    int n;
    int i;
    int arr[ARRAY_SIZE];

    scanf("%d", &n);

    for (i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    if (MergeSort(arr, 0, n - 1) == 0) {
        puts("sort error");
        return 0;
    }

    // for (i = 0; i < n; i++) {
    //     printf("%d ", arr[i]);
    // }

    // puts("");
    return 0;
}
    
int Merge(int *arr, int left, int mid, int right) {
    if (arr == NULL) {
        return 0;
    }
    /* 정렬하여 담을 배열 */
    int *sorted_arr = (int *) malloc(sizeof(int) * right + 1);
    int sorted_idx = left;
    int l_idx = left;
    int r_idx = mid + 1;
    int i;

    while (1) {
        if (l_idx > mid) {
            break;
        }
        else if (r_idx > right) {
            break;
        }

        if (arr[l_idx] < arr[r_idx] + 1) {
            sorted_arr[sorted_idx] = arr[l_idx++];
        }
        else {
            sorted_arr[sorted_idx] = arr[r_idx++];
        }
        sorted_idx++;
    }

    if (l_idx > mid) {
        for (i = r_idx; i < right + 1; i++, sorted_idx++) {
            sorted_arr[sorted_idx] = arr[i];
        }
    }
    else {
        for (i = l_idx; i < mid + 1; i++, sorted_idx++) {
            sorted_arr[sorted_idx] = arr[i];
        }   
    }

    for (i = left; i < right + 1; i++) {
        arr[i] = sorted_arr[i];
    }

    return 1;
}

int MergeSort(int *arr, int left, int right) {
    if (arr == NULL) {
        return 0;
    }

    if (left < right) {
        int i;
        int mid = (left + right) / 2;

        if (MergeSort(arr, left, mid) == 0) {
            return 0;
        }

        for (i = 0; i < right + 1; i ++) {
            printf("%d ", arr[i]);
        }
        puts("");
        if (MergeSort(arr, mid + 1, right) == 0) {
            return 0;
        }

        for (i = 0; i < right + 1; i ++) {
            printf("%d ", arr[i]);
        }
        puts("");
        
        if (Merge(arr, left, mid, right) == 0) {
            return 0;
        }

        for (i = 0; i < right + 1; i ++) {
            printf("%d ", arr[i]);
        }
        puts("");
    }
    return 1;
}
