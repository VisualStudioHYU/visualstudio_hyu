/* 2015-12-22
 * File I/O
 * Write by Yedarm Seong */

#include <stdio.h>
#include <string.h>

int main() {
    /* File pointer for input, output */
    FILE *input = fopen("input.txt", "rt");
    if (input == NULL) {
        puts("File open failed(input.txt)");
    }
    FILE *output = fopen("output.txt", "wt");
    if (output == NULL) {
        puts("File open failed(output.txt)");
    }

    /* Input count number */
    char c = fgetc(input);
    /* First line must digit number, alphabet or etc.. is error */
    if (!isdigit(c)) {
        puts("File's first line error");    
    }
    /* If c is digit, change int type */ 
    int n = atoi(&c);

    /* For input temporary */
    char input_tmp[20];
    char arr[n * 3][20];
    
    /* Variable for loop */
    int i;
    int j;
    /* 'fscanf' error check */
    int ret;

    /* Data wrtie loop */
    for (i = 0; i < 3 * n; i++) {
        if (i % 3 == 0) {
            /* Write blank line */
            if (i != 0) {
                fputc('\n', output);
            }
        }
        
        /* Input from file */
        ret = fscanf(input, "%s", input_tmp);
        /* Input line is error or end of file */
        if (ret == EOF) {
            printf("File write error %dline\n", i + 1);
        }
        
        /* Upper, lower */
        for (j = 0; j < strlen(input_tmp); j++) {
            if (input_tmp[j] > 64 && input_tmp[j] < 91) {
                input_tmp[j] += 32;
            }
            else if (input_tmp[j] > 96 && input_tmp[j] < 123) {
                input_tmp[j] -= 32;
            }
        }
        /* Write to file */
        fprintf(output, "%s\n", input_tmp);
    }

    /* File pointer close in memory */
    fclose(input);
    fclose(output);

    return 0;
 }
