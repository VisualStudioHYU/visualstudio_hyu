/* 2016-01-05
 * Sorted linked list
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>

int main() {
    /* For linked list structure */
    struct Node {
        int data;
        struct Node *next;
    };

    /* Temporary input data */
    int tmp;
    /* Number of node */    
    int count = 0;

    /* Dummy node use */
    struct Node *dummy_head = (struct Node *) malloc(sizeof(struct Node));
    dummy_head->next = NULL;
    /* Indicate Node */
    struct Node *cursor;

    do {
        scanf("%d", &tmp);
        /* If input data is 0, loop break */
        if (tmp == 0) {
            break;
        }

        /* Sorting loop */
        for (cursor = dummy_head;
            cursor->next != NULL; cursor = cursor->next) {
            /* Add node suitably */
            if (cursor->next->data > tmp) {
                struct Node *new_node = \
                (struct Node *) malloc(sizeof(struct Node));
                new_node->data = tmp;
                new_node->next = cursor->next;
                cursor->next = new_node;
                break;
            }
        }

        /* Add node last */
        if (cursor->next == NULL) {
            struct Node *new_node = \
            (struct Node *) malloc(sizeof(struct Node));
            new_node->data = tmp;
            new_node->next = NULL;
            cursor->next = new_node;
        }
    } while (1);

    /* printing data */
    for (cursor = dummy_head->next;
        cursor != NULL; cursor = cursor->next) {
        printf("%d\n", cursor->data);
        count++;
    }
    printf("Number of node: %d\n", count);

    return 0;
}
