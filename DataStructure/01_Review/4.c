/* 2016-01-04
 * Dynamic allocate
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Maximum string length */
#define LEN 20

int main() {
    /* Input capacity number */
    int n;
    scanf("%d", &n);

    /* Input exception check */
    if (n < 0) {
        puts("input error");
        return 0;
    }

    /* Dynamic allocate for */
    char **arr = (char **) malloc(sizeof(char *) * n);
    /* 20 bytes allocate */
    char *tmp = (char *) malloc(sizeof(char) * LEN);
    /* The number of array data */
    int count = 0;
    int i;

    /* Dynamic allocate for  */
    for (i = 0; i < n; i++) {
        arr[i] = (char *) malloc(sizeof(char) * LEN);
    }

    while (1) {
        scanf("%s", tmp);
        /* If input 0, loop break and input exit */
        if (!strcmp(tmp, "0")) {
            break;
        }
        /* If a number of array data same input capacity number,
         * do not input to array anymore */
        else if (count == n) {
            continue;
        }

        /* If strcpy return NULL, string copy error */
        if (strcpy(arr[count], tmp) == NULL) {
            puts("string copy error");
        }
        /* Data count increasing */
        count++;
    }

    /* printing data */
    for (i = 0; i < n; i++) {
        puts(arr[i]);
    }

    return 0;
}
