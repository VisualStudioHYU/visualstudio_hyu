/* 2015-12-22
 * Power function(loop)
 * Write by Yedarm Seong */

#include <stdio.h>

/* LoopPower(n, m) = n^m, for loop */
int LoopPower(int n, int m);

int main() {
    /* 'n' is base, 'm' is exp */
    int n, m;
    /* To n^m, '^' is char */
    char ch;

    /* Input n, m */
    scanf("%d%c%d", &n, &ch, &m);
    /* Print result */
    printf("%d\n", LoopPower(n, m));

    return 0;
}

int LoopPower(int n, int m) {
    /* Base or exp number is 0, x^0 or 0^x, result 1 */
    if (n == 0) {
        return 1; 
    }
    else if (m == 0) {
        return 1;
    }

    /* Variable for loop */
    int i;
    /* This function return value */
    int result = n;

    /* Multiple operation */
    for (i = 1; i < m; i++) {
        result *= n;
    }

    return result;
}
