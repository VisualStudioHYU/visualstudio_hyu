/* 2015-12-22
 * Power function(recursive)
 * Write by Yedarm Seong*/

#include <stdio.h>

/* RecursivePower(n, m) = n^m, recursive */
int RecursivePower(int n, int m);

int main() {
    /* 'n' is base, 'm' is exp */
    int n, m;
    /* To n^m, '^' is char */
    char ch;

    /* Input n, m */
    scanf("%d%c%d", &n, &ch, &m);
    /* Print result */
    printf("%d\n", RecursivePower(n, m));

    return 0;
}

int RecursivePower(int n, int m) {
    /* Base or exp number is 0, x^0 or 0^x, result 1 */
    if (n == 0) {
        return 1;
    }
    else if (m == 0) {
        return 1;
    }

    /* Recursive end */
    if (m == 1) {
        return n;
    }
    
    return n * RecursivePower(n, m - 1);
}
