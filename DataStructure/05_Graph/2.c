/* 2015-02-02
 * Shortest path Baekjoon 1753
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


int InitMatrix(int **arr, int size);
int Connection(int **arr, int a, int b, int cost);
int** FindMinCost(int **arr, int start, int size);
int IsTravel(int *arr, int x, int size);

int main() {
    int i, j;
    int v, e;
    int min_cost;
    int start;
    int a, b, cost;

    scanf("%d %d", &v, &e);
    scanf("%d", &start);
    
    int **arr = (int **) malloc(sizeof(int *) * v);

    for (i = 0; i < v; i++) {
        arr[i] = (int *) malloc(sizeof(int) * v);
    }

    if (InitMatrix(arr, v) == 0) {
        return 0;
    }

    for (i = 0; i < e; i++) {
        scanf("%d %d %d", &a, &b, &cost);
        if (Connection(arr, a - 1, b - 1) == 0) {
            return 0;
        }
    }

    arr = FindMinCost(arr, start, v);

    for (i = 0; i < v; i++) {
        if (arr[i][v - 1] != INT_MAX) {
            printf("%d\n", arr[i][v - 1]);
        }
        else {
            puts("INF");
        }
    }

    // for (i = 0; i < v; i++) {
    //     for (j = 0; j < v; j++) {
    //         printf("%10d ", arr[i][j]);
    //     }
    //     puts("");
    // }

    // for (i = 0; i < v; i ++) {
    //     min_cost = FindMinCost(arr, start - 1, i, v);

    //     if (min_cost != INT_MAX) {
    //         printf("%d\n", min_cost);
    //     }
    //     else {
    //         puts("INF");
    //     }
    // }

    return 0;
}

int InitMatrix(int **arr, int size) {
    if (arr == NULL) {
        return 0;
    }

    int i, j;

    for (i = 0; i < size; i++) {
        memset(arr[i], 0, sizeof(int) * size);
    }
    return 1;
}

int Connection(int **arr, int a, int b, int cost) {
    if (arr == NULL) {
        return 0;
    }

    // arr[a][b] = cost;
    arr[b][a] = cost;
    return 1;
}

int** FindMinCost(int **arr, int start, int size) {
    if (start > size) {
        return 0;
    }

    int i, j;
    int travle_size = 0;
    
    int *travel_arr = (int *) malloc(sizeof(int) * size);
    memset(travel_arr, INT_MAX, sizeof(int) * size);

    int **tmp_arr = (int **) malloc(sizeof(int *) * size);

    for (i = 0; i < size; i++) {
        tmp_arr[i] = (int *) malloc(sizeof(int) * size);
        
        if (i == start) {
            memset(tmp_arr[i], 0, sizeof(int) * size);
        }
        else {
            tmp_arr[i][0] = INT_MAX;
        }
    }

    int current;
    int min = start;
    int is_break = 0;
    
    for (j = 1; j < size; j++) {
        if (IsTravel(travel_arr, min, size) == 1) {
            is_break = j;
            break;
        }
        else if (tmp_arr[min][j - 1] == INT_MAX) {
            is_break = j;
            break;
        }
        else {
            current = min;
        }

        for (i = 0; i < size; i++) {
            if (arr[i][current] != 0) {
                if (tmp_arr[current][j - 1] + arr[i][current]
                    <
                    tmp_arr[i][j - 1]) {
                    tmp_arr[i][j] = tmp_arr[current][j - 1] + arr[i][current];
                }
                else {
                    tmp_arr[i][j] = tmp_arr[i][j - 1];
                }
            }
            else {
                tmp_arr[i][j] = tmp_arr[i][j - 1];
            }
        }

        travel_arr[travle_size++] = current;

        for (i = 0; i < size; i++) {
            if (IsTravel(travel_arr, i, size) == 0) {
                min = i;
                break;
            }
        }


        for (i = 0; i < size; i++) {
            if (IsTravel(travel_arr, i, size) == 1) {
                continue;
            }
            else {
                if (tmp_arr[i][j] < tmp_arr[min][j]) {
                    min = i;
                }
            }
        }
    }

    if (is_break != 0) {
        for (j = is_break; j < size; j++) {
            for (i = 0; i < size; i++) {
                tmp_arr[i][j] = tmp_arr[i][j - 1];
            }
        }
    }

    // for (i = 0; i < size; i++) {
    //     for (j = 0; j < size; j++) {
    //         printf("%10d ", tmp_arr[i][j]);
    //     }
    //     puts("");
    // }

    // int ret = tmp_arr[end][size - 1];

    // for (i = 0; i < size; i++) {
    //     free (tmp_arr[i]);
    // }
    // free(tmp_arr);
    free(travel_arr);

    // return ret; 
    return tmp_arr;
}

int IsTravel(int *arr, int x, int size) {
    if (arr == NULL) {
        return 0;
    }

    int i;

    for (i = 0; i < size; i++) {
        if (arr[i] == x) {
            return 1;
        }
    }

    return 0;
}
