/* 2015-02-02
 * Graph
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

/* 다익스트라 */
int InitMatrix(int **arr, int size);
int Connection(int **arr, int a, int b, int cost);
int FindMinCost(int **arr, int a, int b, int size);
int IsTravel(int *arr, int x, int size);

int main() {
    int n;
    int i;
    int min_cost;
    int start, end;

    scanf("%d", &n);

    int **arr = (int **) malloc(sizeof(int *) * n);

    for (i = 0; i < n; i++) {
        arr[i] = (int *) malloc(sizeof(int) * n);
    }

    InitMatrix(arr, n);
    Connection(arr, 0, 1, 2);
    Connection(arr, 0, 2, 4);
    Connection(arr, 1, 2, 5);
    Connection(arr, 1, 3, 2);
    Connection(arr, 1, 4, 5);
    Connection(arr, 2, 3, 1);
    Connection(arr, 3, 4, 3);

    while (1) {
        scanf("%d %d", &start, &end);

        min_cost = FindMinCost(arr, start, end, n);

        if (min_cost != 0) {
            printf("%d\n", min_cost);
        }
        else {
            puts("Find min cost error!");
        }
    }

    return 0;
}

int InitMatrix(int **arr, int size) {
    if (arr == NULL) {
        return 0;
    }

    int i, j;

    for (i = 0; i < size; i++) {
        memset(arr[i], 0, sizeof(int) * size);
    }
    return 1;
}

int Connection(int **arr, int a, int b, int cost) {
    if (arr == NULL) {
        return 0;
    }

    arr[a][b] = cost;
    arr[b][a] = cost;
    return 1;
}

int FindMinCost(int **arr, int a, int b, int size) {
    if (a > size) {
        return 0;
    }
    else if (b > size) {
        return 0;
    }

    int i, j;
    int travle_size = 0;
    
    int *travel_arr = (int *) malloc(sizeof(int) * size);
    memset(travel_arr, INT_MAX, sizeof(int) * size);

    int **tmp_arr = (int **) malloc(sizeof(int *) * size);

    for (i = 0; i < size; i++) {
        tmp_arr[i] = (int *) malloc(sizeof(int) * size);
        
        if (i == a) {
            memset(tmp_arr[i], 0, sizeof(int) * size);
        }
        else {
            tmp_arr[i][0] = INT_MAX;
        }
    }

    int current;
    int min = a;
    
    for (j = 1; j < size; j++) {
        current = min;

        for (i = 0; i < size; i++) {
            if (arr[i][current] != 0) {
                if (tmp_arr[current][j - 1] + arr[i][current]
                    <
                    tmp_arr[i][j - 1]) {
                    tmp_arr[i][j] = tmp_arr[current][j - 1] + arr[i][current];
                }
                else {
                    tmp_arr[i][j] = tmp_arr[i][j - 1];
                }
            }
            else {
                tmp_arr[i][j] = tmp_arr[i][j - 1];
            }
        }

        travel_arr[travle_size++] = current;

        for (i = 0; i < size; i++) {
            if (IsTravel(travel_arr, i, size) == 0) {
                min = i;
                break;
            }
        }


        for (i = 0; i < size; i++) {
            if (IsTravel(travel_arr, i, size) == 1) {
                continue;
            }
            else {
                if (tmp_arr[i][j] < tmp_arr[min][j]) {
                    min = i;
                }
            }
        }
    }

    // for (i = 0; i < size; i++) {
    //     for (j = 0; j < size; j++) {
    //         printf("%10d ", tmp_arr[i][j]);
    //     }
    //     puts("");
    // }

    int ret = tmp_arr[b][size - 1];

    for (i = 0; i < size; i++) {
        free (tmp_arr[i]);
    }
    free(tmp_arr);

    return ret; 


}

int IsTravel(int *arr, int x, int size) {
    if (arr == NULL) {
        return 0;
    }

    int i;

    for (i = 0; i < size; i++) {
        if (arr[i] == x) {
            return 1;
        }
    }

    return 0;
}
