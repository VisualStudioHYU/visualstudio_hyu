/* 2015-02-12
 * Minimum spanning tree Baekjoon 1197
 * Write by Yedarm Seong */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node {
    int a, b;
    int cost; 
};

long long mst_cost;
int *size_arr;

/* 크루스칼 알고리즘 이용 */
int Connection(struct Node *arr, int a, int b, int cost, int n);
int Compare(const void *arg1, const void *arg2);
int Kruscal(struct Node *arr, int v, int e);
int AddEdge(short **connection_arr, int a, int b, int v, int size);
int Find(short *arr, short find, int size);

int main() {
    int v, e;
    int i;
    int a, b, cost;
    struct Node *arr;

    scanf("%d %d", &v, &e);

    arr = (struct Node *) malloc(sizeof(struct Node) * e);
    
    for (i = 0; i < e; i++) {
        scanf("%d %d %d", &a, &b, &cost);
        if (Connection(arr, a - 1, b - 1, cost, i) == 0) {
            return 0;
        }
    }

    qsort(arr, e, sizeof(struct Node), Compare);

    // for (i = 0; i < e; i++) {
    //     printf("%d %d %d\n", arr[i].a, arr[i].b, arr[i].cost);
    // }

    mst_cost = 0;
    Kruscal(arr, v, e);

    printf("%d", mst_cost);

    return 0;
}

int Connection(struct Node *arr, int a, int b, int cost, int n) {
    if (arr == NULL) {
        return 0;
    }

    arr[n].a = a;
    arr[n].b = b;
    arr[n].cost = cost;

    return 1;
}

int Compare(const void *arg1, const void *arg2) {
    int a = ((struct Node *)arg1)->cost;
    int b = ((struct Node *)arg2)->cost;

    if (a > b) {
        return 1;
    }
    else if (b > a) {
        return -1;
    }
    return 0;    
}

int Kruscal(struct Node *arr, int v, int e) {
    if (arr == NULL) {
        return 0;
    }

    int i;
    int size = v;

    if (size % 2 == 1) {
        size /= 2;
        size++;
    }
    else {
        size /= 2;
    }

    short **connection_arr = (short **) malloc(sizeof(short *) * (size));
    for (i = 0; i < size; i++) {
        connection_arr[i] = (short *) malloc(sizeof(short) * v);
        memset(connection_arr[i], -1, sizeof(short) * v);
    }

    size_arr = (int *) calloc(size, sizeof(int));

    connection_arr[0][0] = arr[0].a;
    connection_arr[0][1] = arr[0].b;
    mst_cost += arr[0].cost;
    size_arr[0] = 2;

    for (i = 1; i < e; i++) {
        if (AddEdge(connection_arr, arr[i].a, arr[i].b, v, size) == 1) {
            mst_cost += arr[i].cost;
        }
    }
}

int AddEdge(short **connection_arr, int a, int b, int v, int size) {
    if (connection_arr == NULL) {
        return 0;
    }
    else if (a < 0) {
        return 0;
    }
    else if (b < 0) {
        return 0;
    }

    int i, j;
    int tmp = -1;
    int tmp_size = 0;

    for (i = 0; i < size; i++) {
        if ((Find(connection_arr[i], a, size_arr[i]) == 1)) {
            if (Find(connection_arr[i], b, size_arr[i]) == 1) {
                return 0;
            }
            else {
                for (j = 0; j < size; j++) {
                    if (i == j) {
                        continue;
                    }

                    if (Find(connection_arr[j], b, size_arr[j]) == 1) {
                        tmp = j;
                        break;
                    }
                }
                if (tmp == -1) {
                    connection_arr[i][size_arr[i]++] = b;
                    return 1;
                }

                for (j = 0; j < size_arr[tmp]; j++) {
                    connection_arr[i][size_arr[i]++] = connection_arr[tmp][j];
                    connection_arr[tmp][j] = -1;
                    tmp_size++;
                }

                size_arr[tmp] -= tmp_size;
                return 1;
            }
        }
        else if ((Find(connection_arr[i], a, size_arr[i]) == 0)) {
            if (Find(connection_arr[i], b, size_arr[i]) == 1) {
                int j;
                for (j = 0; j < size; j++) {
                    if (i == j) {
                        continue;
                    }

                    if (Find(connection_arr[j], a, size_arr[j]) == 1) {
                        tmp = j;
                        break;
                    }
                }

                if (tmp == -1) {
                    connection_arr[i][size_arr[i]++] = a;
                    return 1;
                }

                for (j = 0; j < size_arr[tmp]; j++) {
                    connection_arr[i][size_arr[i]++] = connection_arr[tmp][j];
                    connection_arr[tmp][j] = -1;
                    tmp_size++;
                }

                size_arr[tmp] -= tmp_size;
                return 1;

                }
            else {
                if (connection_arr[i][0] == -1) {
                    connection_arr[i][size_arr[i]++] = a;
                    connection_arr[i][size_arr[i]++] = b;
                    return 1;
                }
            }
        }
    }
    return 1;
}

int Find(short *arr, short find, int size) {
    int i;
    
    for (i = 0; i < size; i++) {
        if (arr[i] == find) {
            return 1;
        }
    }
    return 0;
}
